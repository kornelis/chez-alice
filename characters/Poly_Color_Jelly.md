# Poly Color Jelly
  
A huge blob of merged jelly that have come alive. They might taste nice, but they like the taste of meat!  
  
Inspired by the [Ochre Jelly](https://roll20.net/compendium/dnd5e/Ochre%20Jelly) and [Grey Ooze](https://www.dndbeyond.com/monsters/gray-ooze).  
  
The Challenge can be adjusted by selecting the number of PC's and fiddling with the Challenge Rating (CR).  
  
| Color | Taste |
|-------|-------|
| Red   | Strawberry |
| Orange | Orange |
| Yellow | Lemon |
| Green | Apple |
| Blue | Grapes |
| Purple | Blueberries |
  
*Large ooze, Unaligned*  
  
---
  
**Armor Class** 8  (Natural Armor)  
**Hit Points** See table (depending on encounter difficulty setting)  
**Speed** 10 ft., climb 10 ft.  
  
---

| STR     | DEX     | CON     | INT     | WIS     | CHA     |
| --------|---------|---------|---------|---------|---------| 
| 15 (+2) |  6 (-2) | 14 (+2) |  4 (-3) |  6 (+2) |  2 (-4) |
  
| PC's | Difficulty | CR |   XP | Health | Pseudopod Attacks | Tendril Attacks |
|------|------------|----|------|--------|-------------------|-----------------|
|    6 | Muderous   | 12 | 8550 |    232 |                 3 |              15 |
|    6 | Deadly     | 11 | 7650 |    200 |                 3 |              12 |
|    6 | Hard       | 10 | 6125 |    188 |                 3 |              11 |
|    5 | Deadly     | 10 | 6450 |    180 |                 3 |               8 |
|    5 | Hard       |  8 | 4875 |    174 |                 3 |               6 |
|    4 | Deadly     |  9 | 5700 |    160 |                 2 |              10 |
|    4 | Hard       |  8 | 4250 |    146 |                 2 |               8 |
|    3 | Deadly     |  7 | 3750 |    130 |                 2 |               6 |
|    3 | Hard       |  6 | 2600 |    115 |                 2 |               4 |
|    3 | Medium     |  5 | 2200 |    100 |                 2 |               2 |
|    3 | Easy       |  4 | 1350 |     90 |                 2 |               0 |
  
---
  
**Damage Resistance** Acid  
**Damage Immunities** Lightning, Slashing  
**Condition Immunities** Blinded, Charmed, Deafened, Exhaustion, Frightened, Prone  
**Senses** Blindsight 60 Ft. (Blind Beyond This Radius), passive Perception 8  
**Challenge** 6 ~ 11 (1300 ~ 7650 XP)  
  
---
  
**Amorphous.** The ooze can move through a space as narrow as 1 inch wide without squeezing.  
  
**Spider Climb.** The jelly can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.  
  
**Corrode Metal.** Any nonmagical weapon made of metal that hits the ooze corrodes. After dealing damage, the weapon takes a permanent and cumulative −1 penalty to damage rolls. If its penalty drops to −5, the weapon is destroyed. Nonmagical ammunition made of metal that hits the ooze is destroyed after dealing damage.  
  
The ooze can eat through 2-inch-thick, nonmagical metal in 1 round.  
  
**Split attacks.** The Pseudopod and Tendrilpod attack have their own initiatives. The number of attacks depends on the health remaining.  
    
---
  
###### Actions:
  
**Pseudopod.** *Melee Weapon Attack:* +4 to hit, reach 10 ft., one target. Hit: 9 (2d6 + 2) bludgeoning damage plus 3 (1d6)acid damage.  
  
**Tendrilpod.** *Melee Weapon Attack:* +3  to hit, reach 5 ft., one target. Hit: 4 (1d6 + 2) bludgeoning damage plus 3 (1d6)acid damage.    
  