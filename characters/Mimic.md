# Mimic
  
The table in the Hight Tea Room in Chez Alice. At the DM's discretion it can come alive. Like when the not-tea's are attacked and a hit misses the little thingey's but does hit the table...  
  
*Medium monstrosity (shapechanger), Neutral*  
  
---
  
**Armor Class** 12  (Natural Armor)  
**Hit Points** 58 (9d8+18)  
**Speed** 15 ft.  
  
---

| STR     | DEX     | CON     | INT     | WIS     | CHA     |
| --------|---------|---------|---------|---------|---------| 
| 17 (+3) | 12 (+1) | 15 (+2) |  5 (-3) | 13 (+1) | 8  (-1) |
  
---
  
**Skills** Stealth +5  
**Damage** Immunities Acid  
**Condition** Immunities Prone  
**Senses** Darkvision 60 Ft., passive Perception 11  
**Languages** Common  
**Challenge** 2 (450 XP  
  
---
  
**Shapechanger.** The mimic can use its action to polymorph into an object or back into its true, amorphous form. Its statistics are the same in each form. Any equipment it is wearing or carrying isn 't transformed. It reverts to its true form if it dies.  
  
**Adhesive (Object Form Only).** The mimic adheres to anything that touches it. A Huge or smaller creature adhered to the mimic is also grappled by it (escape DC 13). Ability checks made to escape this grapple have disadvantage.  
  
**False Appearance (Object Form Only).** While the mimic remains motionless, it is indistinguishable from an ordinary object.  
  
**Grappler.** The mimic has advantage on attack rolls against any creature grappled by it.  
    
---
  
###### Actions:
  
**Pseudopod.** Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: (1d8 + 3) bludgeoning damage. If the mimic is in object form, the target is subjected to its Adhesive trait  
  
**Bite.** Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: (1d8 + 3) piercing damage plus (1d8)acid damage.  
  