# Mouse
  
The mice encounterd in Chez Alice follow the rules of commoner while the invitees are tiny size.  
  
*Medium Creature*  
  
---
  
**Armor Class** 10  
**Hit Points** 4  
**Speed** 30 ft.  
  
---

| STR     | DEX     | CON     | INT     | WIS     | CHA     |
| --------|---------|---------|---------|---------|---------| 
| 10 (+0) | 10 (+0) | 10 (+0) | 10 (+0) | 10 (+0) | 10 (+0) |
  
---
  
**Senses** passive Perception 10
**Languages** Common & French (Elven)
**Challenge** 0 (10 XP)
  
---
  
###### Actions:
  
**Tail whip** Melee attack: +2 to hit, reach 5 ft., one target. Hit: (1d4) slashing damage.  
  