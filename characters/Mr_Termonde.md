# Mr. Termonde

[Mr. Termonde][1] is one of the servers at Chez Alice. He has been doing this job for over ten years and is usually able to serve the guests quite well. He also has a Chez Alice silver bracelet.
  
> "Oh my ears and wiskers, how late it is getting!"  
> "How long is forever?, sometimes one second!"  
  
### Rabitfolk

*Small humanoid, Neutral*

---

**Armour class** 12  
**Hit Points** 7  
**Speed** 40ft.  

---

| STR     | DEX     | CON     | INT     | WIS     | CHA     |
| --------|---------|---------|---------|---------|---------| 
|  9 (+3) | 14 (+2) | 10 (+3) | 14 (+0) | 13 (+1) | 13 (+2) |

---
  
**Skills** Perception +5, Stealth +6  
**Senses** Darkvision 60 Ft., passive Perception 15  
**Languages** Common, Elvish, Dwarven, Goblin, as many as needed   
**Challenge** 1/4 (25 XP)  

---

**Keen Hearing and Sight**. Mr. Termonde has advantage on Wisdom (Perception) checks that rely on hearing. 
**Nimble Escape.** Mr. Termonde can take the Disengage or Hide action as a bonus action on each of its turns.  

---

##### Actions

**Switchblade** Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 3 (1d6) piercing damage.  

[1]: https://en.wikipedia.org/wiki/Blanc_de_Termonde
