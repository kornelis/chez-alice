# Cheshire Cat

The [Cheshire Cat][1] is usually the size of a normal cat. But if you meet him while you have shrunk to tiny size, the following stats apply:  
  
(Stats are based on the Werebear and Wheretiger.)

> "Most everyone's mad here."  
> "You may have noticed that I'm not all there myself."
  
### Cheshire Cat

*Large beast, Unaligned*

---

**Armour class** 12  
**Hit Points** 125  
**Speed** 40ft.  

---

| STR     | DEX     | CON     | INT     | WIS     | CHA     |
| --------|---------|---------|---------|---------|---------| 
| 17 (+3) | 12 (+2) | 16 (+3) | 10 (+0) | 13 (+1) | 13 (+2) |

---
  
**Skills** Perception +5, Stealth +4  
**Senses** Darkvision 60 Ft., passive Perception 15  
**Languages** Common, Elvish  
**Challenge** 5 (1,800 XP)  

---

**Keen Hearing and Sight**. The Cheshire Cat has advantage on Wisdom (Perception) checks that rely on hearing or sight.  
**Pounce.** If the Cheshire Cat moves at least 15 feet straight toward a creature and then hits it with a claw attack on the same turn, that target must succeed on a DC 15 Strength saving throw or be knocked prone. If the target is prone, the Cheshire Cat can make one bite attack against it as a bonus action.  
**Invisibility.** When the Cheshire Cat goes below 25 hit points, he turns invisible and goes and find a safe place to lick it's wounds.

---

##### Actions

**Multiattack** The Cheshire Cat has two attacks. It can attack with two claw attacks, or one claw and bite.  
**Bite** Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 15 (2d10 + 4) piercing damage.  
**Claw** Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) piercing damage.  

[1]: https://en.wikipedia.org/wiki/Cheshire_Cat
