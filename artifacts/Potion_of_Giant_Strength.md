# Potion of Giant Strength (Hill Giant)
  
When you drink this potion, your Strength score changes for 1 hour. This potion is of the type of Hill Giant (Str 21). The potion has no Effect on you if your Strength is equal to or greater than that score.  
  
This potion’s transparent liquid has floating in it a sliver of fingernail from a Hill Giant.  
  