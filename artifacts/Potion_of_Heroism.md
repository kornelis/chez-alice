# Potion of Heroism
  
For 1 hour after drinking it, you gain 10 Temporary Hit Points that last for 1 hour. For the same Duration, you are under the Effect of the bless spell (no Concentration required). This blue potion bubbles and steams as if boiling.  
  