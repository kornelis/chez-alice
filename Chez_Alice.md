# Chez Alice

Welcome DM. Let's have some fun! 

 - [Notes for the DM][#notes]
 - [Arrival at Chez Alice][#arrival]
 - [Map of the High Tea Room in Chez Alice][01]

##### <a name="notes">Notes to the DM</a>
  
> Parts like this, quote blocks, are the parts to be read out loud to the players. These can be descriptions or starters for conversation etc.  
  
*Parts in cursive, like this, are things you can ask the players to do.*  
  
Parts in {brackets} are things you will need to fill in yourself as the situation requires.  
  
**Size change** To keep things moderatly simple, while the text might say the characters change shape, for DM purposes the room changes size. So when the players change size to tiny, the room becomes bigger. When the players change size to large, the rooms becomes smaller. This way the monsters and other stuff can be easely adjusted to the need.  


---
  
### <a name="arrival">Arrival at Chez Alice</a>
  
> Chez Alice is a well known upscale restorant. It caters to the upper crust. With a surprisingly large amount of adventures among them.
> The place itself looks very classy with lots of white an blue used in the decor. The empoyees look imequlate. And all is tidy, clean and tastefully lit.

> One by one the invitees arrive. Besides the door they are met by the porter.  
> Good afternoon {madam || sir}, I am the porter of Chez Alice, and who might you be?  
> Ah, yes, thank you, we have been expecting you. Can I take your coat?  
> Please take a seat at the bar till all our invitees have arrived.  
  
*Let the player describe their character.*  
  
> As the character arrive one by one. The gossip in the room increases. By the third arrival the regulars know that a special event is about to start. And so, messagers are being send out, be it magical or not. By the time the last invitee has arrived the room is buzzing with excitement. The invitees are beeing eyed very carefully and if the regulars would have more time they would start asking questions.  
> 
> Then the last invitee arrives, flanked by a female ogre and a very heavy set dwarf, both surprisingly well dressed, and is delivered without any words to the porter.  
> Porter: "Ah, and you must be {charater name}, welcome, how pleasant of you to have made it to our event."  
  
  
  
---

### Introduction by Madam Alice.

> As the last of the invitees takes her / his / its place at the bar a tall human woman enters the room. She is wearing a long blue dress with white trimming. Her hair is blond with silver streaks and is pulled into an elaborate bun. Her eyes are bright blue but also show her age. Her movements are gracefull.  
> When she reaches the invitees, she says: "I am Lady Alice. Welcome to our hight tea. Follow me."  
  
> You are taken into an other large room. The roof is made out of glass, in the middle of it a stained part that seems to tells a story. When you focus on it you see that the people and animals in it are moving. The walls are painted white with six light blue colored colums standing clear and supporting the roof. The floor is made out of white neatly fitted tiles. In the centre of the room is a [large table][14] with six seats.  
  
The table is a mimic. Use wisely.
  
> "Take a seat", Lady Alice commands.  
  
*Let the characters chose where to sit around the table.*  
  
> "Good" continues Lady Alice, "here are the rules:  
>  
> One: you shall not leave this room till all parts of the High Thea have been served.  
> Two: you must partake of every course and dish offered.  
> Three: the last one, or ones, conscious will receive their price.  
>  
> If you wish, you can put this braclet on and I will ensure you will not die, here, today." With these last words the lays six silver braclets on the table and walks out of the room. You can see that Lady Alice is also wearing one of the indicated braclets. She waits a short while and then Lady Alice leaves the room.  
  
The [silver braclets][19] are magical, if put on, will teleport the characters, or any living matter, out of this room if they, or the organic, become unconscious (or without thought). When Lady Alice leaves the room they activate, and so the ones that are not being worn dissapear.
  
  
  
---

# Chez Alice High Tea

There are five courses in this High Tea. Each will have a different flavour and will need a different skillset to survive and overcome. Each round will start with the arrival of the tea and dishes served, one dish will be sweet the other savoury. As stated in the rules, the characters each must drink the tea, and eat, at least one bite, from each of the dishes served.  
  
 - Round One   : Darjeeling
 - Round Two   : Jasmine
 - Round Three : Chamomile
 - Round Four  : Oolong
 - Round Five  : Lady Grey
  
---

### Round One: Darjeeling  

> As Lady Alice is leaving the room a white rabitfolk hops in. He is about 4 foot tall, with ears about a foot long. He wears a waistcoat, that shows a silver chain, and carriers a plate with covered dishes. As he stops near the table, he says: "I am you server for this afternoon, my name is [mr. Termonde][12]. If you need anything, please let me know." As he is speaking a black porcelin [Kettle Bird][21] flies in and pours its tea into empty cups. It hickups one time, spilling some tea and then flying out again.  
> "Now make your self comfortable and take a sip of this lovely [Darjeeling][31]." The rabitfolk tells you.  
> He continues: "The [soup][32] is made from fresh parsnips, leek and some carrots. The [cake][33] is made with chocolate and quail eggs. Enjoy."  
> mr. Termonde places the food on the table, and then leaves the room.  

*Let the character have a chat about the food and if they like it.*  
  
*Roll for perception*  
  
> You begin to notice that the room is getting bigger, as are the cups and other things on the table.  
> As you look to your companions in the room, it seems their chairs are also getting bigger. How strange!  
> The invitees have been shrunk to tiny size, about a foot in height.  

*Are there any comments on this?*  
  
> The door opens and [two mice][18] come running in, yelling: ```Le chat est là, le chat est là, cours pour ta vie!``` (Elvish for: the cat is here, the cat is here, run for your life!).  
> Running in after them comes a [Cheshire Cat][11].  
>  
> **Roll for inititative!**  
  
The players can fight, charm or do other things with the Cheshire Cat. If they do not stop him he will first attack and eat the mice, then will start on them.  
  
When the Cheshire Cat has been defeated / driven off, etc, mr. Termonde will hop in:  
  
> Oh, my, what a mess. Let me get the cleaning help. We might want do something about the seating arrangements, one moment please.  
> An other server comes in with a table, chairs and tableware the correct size for the invitees. It is all placed on top of the table in the center of the room.  
> "If you would like, I can give you a hand to get on top of the table. In any case, to proceed to the next serving, you must be seated on the table." the server says.  

When the players have taken their seats in the new arrangement, we continue to the next round.  
  
  
  
---
  
### Round Two: [Jasmine][34]
  
> As the invitees sit down they find that their dishes are covered with a tea towel, a different color for every invitee.  
> Under every tea towel the invitees find a paper with some writing, a cup with tea, a few [scones][35] and a small [quiche][36]. All propperly sized for them.  
> On the [paper is written][17]:  
>  
> When you have taken your fill,  
> Then proceed, if you please will.  
> Like;  
> Humpty Dumpty sat on a wall,  
> Humpty Dumpty had a great fall.    
> Four-score Men and Four-score more,  
> Could not make Humpty Dumpty where he was before.  
  
The food an drinks puts the eater under the spell of *Feather Fall*, and so, if they jump of the table, will not hurt themselfs. If they otherwise make a fall, that deals damage as normal (2D6). If the players are not able to solve the riddle, they can use *insight* or *history* to get hints or knowlidge on what to do.  
  
When the invitees land on the floor they will find a small table with a basket on it. In the bascet are a number of small bottles, one less then the number of invitees, with on the label: *drink me*. The liquit in the bottles have no color, but smell like whine. But taste like banana (if the invitees have ever tasted that) If they do drink it, they will turn their normal size again.
  
  
  
---

### Round Three: [Chamomile][37]
  
> [Mr. Termonde][12] hops into the room and he says: "Please take your seats, the next course will be served in just a moment.  
> A few non discript waiters come in, one for every invitee, and serve the hot [Chamomile tea][37] with [Bonbons][38] and [Muffins][39].  
> "Now", mr Termonde continues, "please read over the next text, and explain to Lady Alice what it means. She will be here shortly."
> And he hands them a [piece of paper each][20].  
  
Let each invitee try and explain a part of the poem, in order of height, starting with the smallest one. They may chose what part to explain, but no part can be explainded twice.  
  
> Lady Alice enters the room and says: "Now, who can explain what it means? You the smallest one, please start."  
  
  
  
---

### Round Four: [Oolong][40]
  
> Witout warning the some non descript waiters come in, could be some other, could be the same as last time. They set down a tea kettle, a large number of tea cups and some food.  
> When all is placed, [Mr. Termonde][12] hops in and says: "The tea is Oolong, very, very nice oolong tea. Served with it are brownies and sandwiches. Please enjoy." And he hops out.  

A riddle / puzzle with dancing creepy tea cups, called [not-tea's][13]. There are 3 of these cups to each invitee. These cups start to dance and sing:
  
> After the door has been closed, the tea cups start to move, no, dance, and they start to sing:  
    
> Tea, tea where is the tea?  
> Tea, tea, for you and for a fee.  
> Tea, tea, I wish you could see,  
> Tea, tea, is not here, it's me!  
>  
> Tea, tea, do you have some?  
> Tea, tea, do you have some?  
> Tea, tea, do you have some for me?  
>  
> Tea is our love,  
> Tea is our live,  
> Please give us some tea,  
> Before you must flee!  
>  
> Tea, tea, do you have some?  
> Tea, tea, do you have some?  
> Tea, tea, do you have some for me?  
>    
> Tea is like math,  
> And to math is to love,  
> Solve our math,  
> And we'll be like tea to you.  
  
On a 20+ perception check the invitees will be able to spot that the cups can be bribed with food or tea. Or see that the table is a [mimic][14].  
  
> In front of every invitee there now stands 3 animated not-tea's.  
> They say in unison: "Please answer our riddles!"
  
**Roll for insight to see who goes first, there will be 2 rounds."**  
**Roll a D12, for each invitee for every round.** (see next table for the puzzle to answer)  
    
The invitees might be able to answer a question on their own, or roll for intelligence or wisdom on the first table and insight on the second, see the tables for the riddles. If the [answers for the second table][41] does not make sense, check the link.
  
First Riddle Tables (Intelligenge or Wisdom)  
| D12	| Riddle								| Answer | Int or Wis   |
|-------|---------------------------------------|-------|-----|
| 1		| 2 fathers and 2 sons sat down to eat eggs for breakfast. They ate exactly 3 eggs, each person had 1 egg. | Grand father, father and son | 5+ |
| 2		| What do mathematics teachers like to eat? | π (Pie) | 5+ |
| 3		| What does one math book say to another? | I have so many problems | 5+ |
| 4		| It took 12 men 12 hours to construct a wall. Then how long will it take for 6 men to complete the same wall? | No time! There is no need of constructing it again as the job is already done. | 5+ |
| 5		| What can you put between 7 and 8, so that the result is greater than 7, but less than 8? | A decimal point | 10+ |
| 6		| In a circle, how many sides are there? | There are two sides in a circle. Inside and outside | 10+ |
| 7		| A triangle and circle are friends. What did the triangle might have said to the circle? | Circle, you are pointless. | 10+ |
| 8 	| How many eggs can you put in an empty basket of 2m x 2m size? | Only one! After that, the basket is not empty | 10+ |
| 9		| How many times can you subtract the number 5 from 25 ? | Once, because after you subtract it's not 25 anymore. | 10+ |
| 10	| Lily is a lilypad in a small pond. Lilly doubles her size each day, On the 20th day she covers the whole pond. On what day was Lily half the size of the pond? | Day 19, it's not 10 because on day 20 she doubled from day 19, so 19 must be half the size of the pond. | 10+ |
| 11	| Why is it easier to count cows than sheep? | You can use a cowculator. | 10+ |
| 12	| What happened to the mathematician plant? | It grew square roots, of course. | 10+ |
  
Second Table (Insight)  
| D12	| Riddle								| Answer | Insight roll |
|-------|---------------------------------------|-------|-----|
| 1		| 0 and 1 make?							| 1		| 5+ |
| 2		| 1 and 1 make?							| 1 & 10 | 5+ |
| 3		| 10 and 01 make?						| 3 & 11 | 5+ |
| 4		| True and False make?					| False | 10+ |
| 5		| False and False make?					| False | 10+ |
| 6		| True and True make?					| True	| 10+ |
| 7		| Can a baby be at the same time a pig? | False | 15+ |
| 8		| Can a baby be at the same time a pig? | True  | 15+ |
| 9		| 4 and 4 make 10, true?				| True & False | 15+ |
| 10	| How much is 4 times 5 ?				| 12	| 20+ |
| 11	| How much is 4 times 6 ?				| 13	| 20+ |
| 12	| How much is 4 times 7 ?				| 14	| 20+ |
  
> After the riddles the not-tea's dance in a line out of the room.  
  
In the next round there will be items placed before the invitee according to how well they did with the riddles. Do consider roleplay as a bonus!
  
| Place | Price won | Where to find description |
|-------|-----------|---------------------------|
|     1 | Potion of Invulnerability | [DMG 188][22] |
|     2 | Potion of Resistance (Acid) | [DMG 188][23] |
|     3 | Potion of Heroism | [DMG 188][24] |
|     4 | Potion of Giant Strength | [DMG 187][25] |
  
  
  
---
  
### Round Five: [Lady Grey][51]
  
> Mr Termonde hops in and says: "I hope you had a nice round? Yes? Well, it's time for our last and final round. We will serve Lady Grey tea with jelly deserts in different shapes and sizes and chicken parcels. Please enjoy."  
> He places before the invitees some potions in glass. "Compliments of the not-tea cups." he says. The bottles have labels of their contents.  
> Behind him a serie of waiters comes in and places the food on the table. As well as empty tea cups. They then all leave, quite fast, as a white and blue kettle bird comes flying in. It seems to be in a bit of a hurry. It starts to pour out the tea, but it's aim is off and the amount of tea comming out is too much for the small tea cups.  
> As the kettle bird empies out over the table, hitting almost everyting on and near the table. Not only is the bird making a huge mess, the jellies start to grow.  
> The invitees also start to grow, unless they succeed in a **15+ dexterity check**.  
> The mess of Jellies, better described as a [Poly Color Jelly][15] rears up and starts attacking everything near, starting with the kettle bird.  
  
**Roll for inititative!**  
  
Combat will continue till the Poly Color Jelly is defeated or if there is only one invitee consious, what ever comes first.  
   
   
   
### After Tea
  
Lady Alice comes in to the room, pours a liquit on the the Poly Color Jelly to bring it back to size again. Then she congratulates the winner and gives him / her / it 500 gp or an uncommen magic item as they wish. The winner is escorted out of the room by mr. Termonde. From now on they are allowed access to Chez Alice.  
  
The other invitees might be still alive, if they have used the silver braclets.  
  
  
  
[00]: https://www.librije.com/
[01]: ./locations/High_Tea_Room.png
[11]: ./characters/Cheshire_Cat.md
[12]: ./characters/Mr_Termonde.md
[13]: ./characters/Not_Tea.md
[14]: ./characters/Mimic.md
[15]: ./characters/Poly_Color_Jelly.md
[17]: ./artifacts/Humpty_Dumpty.md
[18]: ./characters/Mouse.md
[19]: ./Alice_s_Bracelets.md
[20]: ./artifacts/Jabberwockey.md
[21]: https://www.reddit.com/r/DnD/comments/gblulr/a_low_level_magic_item_my_players_love_and_yours/
[22]: ./artifacts/Potion_of_Invulnerability.md
[23]: ./artifacts/Potion_of_Resistance.md
[24]: ./artifacts/Potion_of_Heroism.md
[25]: ./artifacts/Potion_of_Giant_Strength.md
[31]: https://en.wikipedia.org/wiki/Darjeeling_tea
[32]: https://duckduckgo.com/?q=soup+with+parsnips+leek+and+carrots
[33]: https://duckduckgo.com/?q=cake+chocolate+and+quail+eggs
[34]: https://en.wikipedia.org/wiki/Jasmine_tea
[35]: https://duckduckgo.com/?q=scones+and+tea
[36]: https://duckduckgo.com/?q=small+quiche
[37]: https://en.wikipedia.org/wiki/Matricaria_chamomilla
[38]: https://duckduckgo.com/?q=bonbons
[39]: https://duckduckgo.com/?q=muffins
[40]: https://en.wikipedia.org/wiki/Oolong
[41]: https://io9.gizmodo.com/a-math-free-guide-to-the-math-of-alice-in-wonderland-5907235
[51]: https://en.wikipedia.org/wiki/Lady_Grey_(tea)
[52]: https://duckduckgo.com/?q=brownies
[53]: https://duckduckgo.com/?q=sandwiches
