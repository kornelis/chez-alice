# Chez Alice

Chez Alice is a one shot (shot story) for [Dungeons & Dragons 5e][1]. It is inspired by [Through the Looking-Glass, and What Alice Found There][2], a novel by Lewis Carroll published in the last week of 1871. D&D being what it is in 5e, you might also take a peak at what a certain [Tim Burton][3] has done with the story.


### The Adventure

Follows D&D 5e rules. Is written for four players, each with a level 5 character. But should be easely adjusted for more or less players.  
  
  
### For players  
  
Chez Alice is an upscale restaurant in Waterdeep. Every season it holds a special event. This event is a contest during a lunch, dinner or some other dining. The rules are simple; keep the etiquette, solve the puzzles and survive the dishes served. The winner, by being the last to be conscious or by some other means, wins a large sum of gold or a magical item of same size, what ever the attendant choses. To attent this event you need to enter into a lottery. Entrance price to the lottery is 10 golden coins. Even though this is a dangerous event, hundreds of people enter this lottery every season. Most of them willingly. If, and only if, you receive one of the invitations you are allowed to attend.  
  
And so, you have found yourself in the possession of an invitation to this years **Chez Alice Summer High Tea**!  
  
The ticket reads:

---

>
> **Dear madam or sir,**
>
> You are requested to attends Madam Alice's Summer High Tea. You are expected to be there tomorrow before tea time. Dress code is formal, as is for all dining with Chez Alice. Failure to be on time will reciprocated by putting forth a reward to enlighten you and others of the errors of your ways.
>

### For the Dungeon Master

**If you are a player, stop reading here!!!** If you are a DM, please [continue][4].


[1]: https://dnd.wizards.com/
[2]: https://en.wikipedia.org/wiki/Through_the_Looking-Glass
[3]: https://duckduckgo.com/?q=tim+burton+alice+in+wonderland&t=h_&iax=images&ia=images
[4]: ./Chez_Alice.md
